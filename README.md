## 数据模糊搜索匹配
1. 根据用户文本框输入的内容，查询对应的结果列表。
2. 案例一：使用的是H5最新的标签 datalist，会有兼容问题
3. 案例二：使用html ul + li 标签实现
 
### 项目环境
Apache + mysql + php

### 使用帮助：
1. 项目根路径下 test.sql 文件导入数据库中
2. 将源代码放入web服务器中的目录（默认htdocs/www/wwwroot等）
3. 在项目代码中/application/database.php中修改数据库的配置文件
4. 访问项目：按案例进行访问
5. 输入 数据进行测试（秋、志、等等）。

### 演示
#### 案例一：
http://127.0.0.1/0502/public/index.php/Index/index
![案例一演示图](https://git.oschina.net/uploads/images/2017/0503/104031_a6e2977e_559019.gif "在这里输入图片标题")
#### 案例二：
http://127.0.0.1/0502/public/index.php/Index/index/case2
![案例二演示图](https://git.oschina.net/uploads/images/2017/0503/132618_23ab6bc7_559019.gif "在这里输入图片标题")



